<?php

namespace Drupal\representative_image\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Fetches representative image fields from the source database.
 *
 * @MigrateSource(
 *   id = "d7_representative_image_field_storage_config",
 *   source_module = "representative_image",
 * )
 */
class FieldStorageConfig extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Fetch representative image fields that have a value.
    return $this->getDatabase()
      ->select('variable', 'v')
      ->fields('v', ['name', 'value'])
      ->condition('name', 'representative_image_field_%', 'LIKE')
      ->condition('value', 's:0:"";', '<>')
      ->condition('value', 'i:0;', '<>');
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'name' => $this->t("The name of the representative image field's variable."),
      'value' => $this->t("The value of the representative image field's variable."),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $name = $row->getSourceProperty('name');
    // Variables are made of representative_image_field_[entity type]_[bundle].
    // First let's find a matching entity type from the variable name.
    foreach ($entity_definitions as $entity_type => $definition) {
      if (strpos($name, 'representative_image_field_' . $entity_type . '_') === 0) {
        // Set process values.
        $row->setSourceProperty('entity_type', $entity_type);
        return parent::prepareRow($row);
      }
    }

    // No matching entity type found in destination for this variable. Skipping.
    return FALSE;
  }

}
