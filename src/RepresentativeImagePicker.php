<?php

namespace Drupal\representative_image;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\ListDataDefinitionInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\representative_image\Exception\RepresentativeImageFieldNotDefinedException;

/**
 * Finds representative image fields on entities.
 */
class RepresentativeImagePicker {
  use StringTranslationTrait;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a RepresentativeImagePicker object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, ModuleHandlerInterface $module_handler) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Gets the representative image field item from a representative image field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   A field item list containing a representative image field.
   *
   * @throws \LogicException
   *   Thrown when $items does not contain a representative image field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   Returns the representative image field items on the entity for the
   *   given representative_image field, or NULL if one can't be found.
   */
  public function getImageFieldItemList(FieldItemListInterface $items) {
    $field_definition = $items->getFieldDefinition();

    if ($field_definition->getType() !== 'representative_image') {
      throw new \LogicException('Items does not contain a representative image field.');
    }

    $field_name = $field_definition->getSetting('representative_image_field_name');
    $entity = $items->getEntity();
    if (!empty($field_name) && $entity->hasField($field_name) && !$entity->get($field_name)->isEmpty()) {
      $image_field_items = $entity->get($field_name);
    }
    else {
      $behavior = $field_definition->getSetting('representative_image_behavior');
      if ($behavior === 'nothing') {
        return NULL;
      }
      else {
        if ($behavior === 'first') {
          $image_field_items = $this->getFirstAvailableImageField($entity);
        }
        elseif ($behavior === 'first_or_default') {
          $image_field_items = $this->getFirstAvailableImageField($entity) ?? $this->getDefaultImage($items);
        }
        else {
          $image_field_items = $this->getDefaultImage($items);
        }
      }
    }

    // Allow modules to alter the image field item list.
    if ($image_field_items) {
      $this->moduleHandler->alter('representative_image', $image_field_items, $field_definition, $entity);
    }

    // We got a field item, but it's a reference to another entity such as a
    // media entity.
    if ($image_field_items) {
      return $this->getImageFieldFromReference($image_field_items);
    }

    return NULL;
  }

  /**
   * Returns the first image field containing an image in the entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity being viewed.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field item or NULL if not found.
   */
  protected function getFirstAvailableImageField(FieldableEntityInterface $entity) {
    $field_item = NULL;
    foreach ($this->getSupportedFields($entity->getEntityTypeId(), $entity->bundle()) as $field_id => $field_label) {
      if (!$entity->get($field_id)->isEmpty() && ($this->getImageFieldFromReference($entity->get($field_id)))) {
        $field_item = $entity->get($field_id);
        break;
      }
    }

    return $field_item;
  }

  /**
   * Finds supported image fields to use as representative field.
   *
   * Supported fields include image fields (which themselves are entity
   * references to file entities), or generic entity reference fields. In this
   * case, it is expected that the referenced entity type has it's own
   * representative image field configured.
   *
   * @param string $entity_type
   *   The entity type name.
   * @param string $bundle
   *   The bundle name.
   *
   * @return array
   *   An associative array with field id as keys and field labels as values.
   */
  public function getSupportedFields($entity_type, $bundle) {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $options = [];
    foreach ($field_definitions as $field_id => $field_definition) {
      if (in_array($field_definition->getType(), ['image', 'entity_reference']) && $field_definition instanceof ListDataDefinitionInterface) {
        $target_entity_type = $field_definition->getItemDefinition()->getSetting('target_type');
        $definition = $this->entityTypeManager->getDefinition($target_entity_type);
        if (is_a($definition->getClass(), ContentEntityInterface::class, TRUE)) {
          $options[$field_id] = $this->t(':label (@id)', [
            ':label' => $field_definition->getConfig($bundle)->label(),
            '@id' => $field_id,
          ]);
        }
      }
    }

    return $options;
  }

  /**
   * Returns the default field image.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The item list.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field item list or NULL if not found.
   */
  protected function getDefaultImage(FieldItemListInterface $items) {
    $default_image = $items->getFieldDefinition()->getSetting('default_image');
    // If we are dealing with a configurable field, look in both
    // instance-level and field-level settings.
    if (empty($default_image['uuid']) && $items->getFieldDefinition() instanceof FieldConfigInterface) {
      $default_image = $items->getFieldDefinition()->getFieldStorageDefinition()->getSetting('default_image');
    }
    if (!empty($default_image['uuid']) && $file = $this->entityRepository->loadEntityByUuid('file', $default_image['uuid'])) {
      // Clone the FieldItemList into a runtime-only object for the formatter,
      // so that the fallback image can be rendered without affecting the
      // field values in the entity being rendered.
      $items = clone $items;
      $items->setValue([
        'target_id' => $file->id(),
        'alt' => $default_image['alt'],
        'title' => $default_image['title'],
        'width' => $default_image['width'],
        'height' => $default_image['height'],
        'entity' => $file,
        '_loaded' => TRUE,
        '_is_default' => TRUE,
      ]);
    }

    return $items;
  }

  /**
   * Find a representative image field by following entity references.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $list
   *   Field list.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   Returns the representative image field items on the entity by
   *   traversing the given field list if it's an entity reference. If the
   *   field is not an entity reference it's simply returned.
   */
  protected function getImageFieldFromReference(FieldItemListInterface $list) {
    if (!$this->fieldItemIsEntityReference($list)) {
      return $list;
    }

    if ($list instanceof EntityReferenceFieldItemListInterface) {
      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      foreach ($list->referencedEntities() as $entity) {
        if ($this->hasRepresentativeImageField($entity)) {
          $representative_field = $this->getRepresentativeImageField($entity);
          return $this->getImageFieldItemList($representative_field);
        }
      }
    }

    return NULL;
  }

  /**
   * Return if the field is not an image field and is not empty.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_item
   *   The field item to check.
   *
   * @return bool
   *   TRUE if the field is not an image field and has a value.
   */
  private function fieldItemIsEntityReference(FieldItemListInterface $field_item): bool {
    return ($field_item instanceof EntityReferenceFieldItemListInterface) && $field_item->getFieldDefinition()->getType() != 'image' && !$field_item->isEmpty();
  }

  /**
   * Return if the entity has a representative image field defined.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to find the representative image field on.
   *
   * @return bool
   *   TRUE if the field is defined, false otherwise.
   */
  public function hasRepresentativeImageField(FieldableEntityInterface $entity) {
    try {
      $this->getRepresentativeImageField($entity);
      return TRUE;
    }
    catch (RepresentativeImageFieldNotDefinedException $e) {
      // Fall through to the FALSE return.
    }
    return FALSE;
  }

  /**
   * Finds the representative image field in an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   An entity instance.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The representative image field.
   */
  public function getRepresentativeImageField(FieldableEntityInterface $entity) {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($field_definitions as $field_id => $field_definition) {
      if ($field_definition->getType() === 'representative_image') {
        return $entity->get($field_id);
      }
    }

    throw new RepresentativeImageFieldNotDefinedException();
  }

  /**
   * Wrapper function to retrieve field items.
   *
   * If there is a valid representative image field return field items or null.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity that contains a representative image field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   Returns the representative image field items on the entity for the given
   *   representative_image field, or NULL if one can't be found.
   */
  public function getImageFieldItems(FieldableEntityInterface $entity): ?FieldItemListInterface {
    if ($this->hasRepresentativeImageField($entity)) {
      $representative_image_field = $this->getRepresentativeImageField($entity);
      return $this->getImageFieldItemList($representative_image_field);
    }

    return NULL;
  }

  /**
   * Gets the URI of the image that is set for an entity's representative image.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity that contains a representative image field.
   * @param array $attributes
   *   An optional array of attributes. The following attributes may be added
   *   to this array by reference:
   *   - width: the width of the image in pixels.
   *   - height: the height of the image in pixels.
   *   - alt: the alternate text of the image.
   *   - title: the title text of the image.
   *
   * @return string|null
   *   The file URI of the representative image.
   */
  public function getImageFromEntity(FieldableEntityInterface $entity, array &$attributes = []): ?string {
    $uri = NULL;

    $image_items = $this->getImageFieldItems($entity);
    if ($image_items && !$image_items->isEmpty()) {
      // Load the attributes from the image field value if available.
      foreach (['width', 'height', 'alt', 'title'] as $attribute) {
        if (empty($attributes[$attribute]) && !empty($image_items->{$attribute})) {
          $attributes[$attribute] = $image_items->{$attribute};
        }
      }

      /** @var \Drupal\file\FileInterface $entity */
      $entity = $image_items->entity;
      // Get the image URI from the file entity.
      $uri = $entity->getFileUri();
    }

    return $uri;
  }

}
