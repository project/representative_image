<?php

/**
 * @file
 * API integration for the representative_image module.
 */

/**
 * Alter the representative image field item list.
 *
 * @param \Drupal\Core\Field\FieldItemListInterface $items
 *   The image field items containing the representative image data.
 * @param \Drupal\Core\Field\FieldDefinitionInterface $representative_image_field
 *   The representative_image type field.
 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
 *   The source entity.
 *
 * @return void
 */
function hook_representative_image_alter(\Drupal\Core\Field\FieldItemListInterface $items, \Drupal\Core\Field\FieldDefinitionInterface $representative_image_field, \Drupal\Core\Entity\FieldableEntityInterface $entity): void {
  // If the image has empty alt text, load in the field_alt_text value.
  if ($entity->hasField('field_alt_text')) {
    if (!empty($items[0]->alt) && !$entity->get('field_alt_text')->isEmpty()) {
      $items[0]->alt = $entity->get('field_alt_text')->getString();
    }
  }
}
