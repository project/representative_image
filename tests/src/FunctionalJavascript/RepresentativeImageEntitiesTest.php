<?php

namespace Drupal\Tests\representative_image\FunctionalJavascript;

/**
 * Test that entities can have associated representative image fields.
 *
 * @group representative_image
 */
class RepresentativeImageEntitiesTest extends RepresentativeImageTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'file',
    'image',
    'node',
    'representative_image',
  ];

  /**
   * Confirm that the defaults are sensible out of the box.
   */
  public function testDefaults() {
    $image1 = $this->randomFile('image');
    $image2 = $this->randomFile('image');

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // 1. Set the first image field as the representative.
    $edit = [
      'settings[representative_image_field_name]' => 'field_image1',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');

    // Create a node with an image in field_image1. Check that it is shown.
    $this->drupalGet('node/add/article');
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->attachFileToField('files[field_image1_0]', $this->fileSystem->realpath($image1->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image1_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image1[0][alt]', $this->randomMachineName());
    $page->pressButton('Save');
    $assert_session->responseContains($image1->name);

    // 2. Set representative image to fall back to the first available image
    // found in the entity.
    $edit = [
      'settings[representative_image_behavior]' => 'first',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');

    // Create a node with an image in the second image field. Check that it is
    // shown.
    $this->drupalGet('node/add/article');
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->attachFileToField('files[field_image2_0]', $this->fileSystem->realpath($image2->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image2_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image2[0][alt]', $this->randomMachineName());
    $page->pressButton('Save');
    $assert_session->responseContains($image2->name);

    // 3. Set the fallback to first image found or default image.
    $edit = [
      'settings[representative_image_behavior]' => 'first_or_default',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');

    // Create a node without images.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
    ];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $assert_session->responseContains($this->defaultImageFile->name);
  }

}
