<?php

namespace Drupal\Tests\representative_image\FunctionalJavascript;

/**
 * Test the token integration of representative images.
 *
 * @group representative_image
 */
class RepresentativeImageTokenTest extends RepresentativeImageTestBase {

  /**
   * Tests custom tokens.
   *
   * Test that the custom tokens defined by representative image return the
   * expected values.
   */
  public function testTokenIntegration() {
    /** @var \Drupal\Core\Utility\Token $token */
    $token = \Drupal::token();

    $image1 = $this->randomFile('image');
    $image2 = $this->randomFile('image');

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Create an article node for testing.
    $this->drupalGet('node/add/article');
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->attachFileToField('files[field_image1_0]', $this->fileSystem->realpath($image1->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image1_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image1[0][alt]', $this->randomMachineName());
    $result = $assert_session->waitForElementVisible('css', '[name="files[field_image2_0]"]:enabled');
    $this->assertNotEmpty($result);
    $page->attachFileToField('files[field_image2_0]', $this->fileSystem->realpath($image2->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image2_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image2[0][alt]', $this->randomMachineName());
    $page->pressButton('Save');

    // Confirm that the correct image is being replaced properly.
    // 1. Set the first image field as the representative.
    $edit = [
      'settings[representative_image_field_name]' => 'field_image1',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');

    // Check that the first image is shown in a processed token.
    $node = $this->nodeStorage->load(1);
    $replacement = $token->replace("foo [node:representative_image] bar", ['node' => $node]);
    $this->assertTrue(strpos($replacement, $image1->name) !== FALSE);

    // 2. Switch the representative image and confirm the representative image
    // is being replaced properly.
    $edit = [
      'settings[representative_image_field_name]' => 'field_image2',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');
    // @todo Need to do this or
    //   RepresentativeImagePicker::getRepresentativeImageField() will return
    //   ''.
    drupal_flush_all_caches();
    $this->nodeStorage->resetCache([1]);

    // Check that the second image is shown in a processed token.
    $node = $this->nodeStorage->load(1);
    $replacement = $token->replace("foo [node:representative_image] bar", ['node' => $node]);
    $this->assertTrue(strpos($replacement, $image2->name) !== FALSE);
  }

}
