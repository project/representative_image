<?php

namespace Drupal\representative_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Representative Image' formatter.
 *
 * @FieldFormatter(
 *   id = "representative_image",
 *   label = @Translation("Representative Image"),
 *   field_types = {
 *     "representative_image",
 *   },
 * )
 */
class RepresentativeImageFormatter extends ImageFormatter {

  /**
   * The representative image picker.
   *
   * @var \Drupal\representative_image\RepresentativeImagePicker
   */
  protected $representativeImagePicker;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->representativeImagePicker = $container->get('representative_image.picker');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $image_items = $this->representativeImagePicker->getImageFieldItemList($items);

    if (!$image_items || $image_items->isEmpty()) {
      return $element;
    }

    $settings = $this->getSettings();
    $element[] = [
      '#theme' => 'image_formatter',
      '#image_style' => $settings['image_style'],
      '#item' => $image_items,
    ];

    if (!empty($settings['image_link'])) {
      if ($settings['image_link'] === 'content') {
        $element['#url'] = $items->getEntity()->toUrl()->toString();
      }
      elseif ($settings['image_link'] === 'file') {
        /** @var \Drupal\Core\Entity\EntityInterface $image_entity */
        $image_entity = $image_items->entity;
        $element['#url'] = $image_entity->toUrl()->toString();
      }
    }

    return $element;
  }

}
