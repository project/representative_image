<?php

namespace Drupal\representative_image\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Representative Image' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_representative_image",
 *   label = @Translation("Representative Image"),
 *   field_types = {
 *     "entity_reference"
 *   },
 * )
 */
class EntityReferenceRepresentativeImage extends EntityReferenceFormatterBase {

  /**
   * The representative image picker.
   *
   * @var \Drupal\representative_image\RepresentativeImagePicker
   */
  protected $representativeImagePicker;

  /**
   * The field formatter plugin manager.
   *
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected $fieldFormatterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->representativeImagePicker = $container->get('representative_image.picker');
    $instance->fieldFormatterManager = $container->get('plugin.manager.field.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $settings = parent::defaultSettings();
    $settings['type'] = 'image';
    $settings['settings'] = [];
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    // This field can only be used if there is a representative image being
    // used on the target entity type.
    $entity_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    $representative_image_fields = \Drupal::service('entity_field.manager')->getFieldMapByFieldType('representative_image');
    return parent::isApplicable($field_definition) && !empty($representative_image_fields[$entity_type]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['type'] = [
      '#type' => 'select',
      '#options' => $this->fieldFormatterManager->getOptions('image'),
      '#title' => t('Image formatter'),
      '#default_value' => $this->getSettingFromFormState($form_state,'type'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'onFormatterTypeChange'],
        'wrapper' => 'representative-image-entity-reference-formatter-settings',
      ],
    ];

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#prefix' => '<div id="representative-image-entity-reference-formatter-settings">',
      '#suffix' => '</div>',
    ];

    $plugin = $this->getImageFieldFormatter($this->getSettingFromFormState($form_state, 'type'), $this->getSettingFromFormState($form_state, 'settings'));
    $form['settings'] += $plugin->settingsForm($form['settings'], $form_state);

    return $form;
  }

  /**
   * Ajax callback for fields with AJAX callback to update form substructure.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The replaced form substructure.
   */
  public function onFormatterTypeChange(array $form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    // Dynamically return the dependent ajax for elements based on the
    // triggering element. This shouldn't be done statically because
    // settings forms may be different, e.g. for layout builder, core, ...
    if (!empty($triggeringElement['#array_parents'])) {
      $subformKeys = $triggeringElement['#array_parents'];
      // Remove the triggering element itself and add the 'settings' below key.
      array_pop($subformKeys);
      $subformKeys[] = 'settings';
      // Return the subform:
      return NestedArray::getValue($form, $subformKeys);
    }
  }

  /**
   * Get the image field formatter plugin.
   *
   * @param string $id
   *   The field formatter plugin ID.
   * @param array $settings
   *   The optional field formatter settings.
   *
   * @return \Drupal\Core\Field\FormatterInterface
   *   The image field formatter plugin.
   */
  protected function getImageFieldFormatter(string $id, array $settings = []): FormatterInterface {
    // Because we don't yet know what the actual field used will be here,
    // we need to create a temporary image field definition.
    $field_definition = BaseFieldDefinition::create('image');
    $field_definition->setName('_' . $this->fieldDefinition->getName() . '_representative_field');
    return $this->fieldFormatterManager->createInstance(
      $id,
      [
        'field_definition' => $field_definition,
        'settings' => $settings,
        'label' => 'Representative Image',
        'view_mode' => '_custom',
        // @todo How to support third party settings?
        'third_party_settings' => $this->thirdPartySettings,
      ],
    );
  }

  /**
   * Retrieve the formatter's getSetting() value from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param string $key
   *   The setting key to retrieve.
   *
   * @return mixed
   *   The form state value or the default setting.
   */
  protected function getSettingFromFormState(FormStateInterface $form_state, string $key) {
    $field_name = $this->fieldDefinition->getName();
    return $form_state->getValue([
      'fields',
      $field_name,
      'settings_edit_form',
      'settings',
      $key,
    ], $this->getSetting($key));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $plugin = $this->getImageFieldFormatter($this->getSetting('type'), $this->getSetting('settings'));
    $summary[] = $this->t('Formatter: @formatter', ['@formatter'=> $plugin->getPluginDefinition()['label']]);
    $summary = array_merge($summary, $plugin->settingsSummary());

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity instanceof FieldableEntityInterface && $this->representativeImagePicker->hasRepresentativeImageField($entity)) {
        $representative_image_field = $this->representativeImagePicker->getRepresentativeImageField($entity);
        if ($representative_image = $this->representativeImagePicker->getImageFieldItemList($representative_image_field)) {
          $elements[$delta] = $representative_image->view([
            'label' => 'hidden',
            'type' => $this->getSetting('type'),
            'settings' => $this->getSetting('settings'),
          ]);
        }
      }
    }

    return $elements;
  }

}
