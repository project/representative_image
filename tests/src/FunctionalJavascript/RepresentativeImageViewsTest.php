<?php

namespace Drupal\Tests\representative_image\FunctionalJavascript;

/**
 * Test the views integration of representative images.
 *
 * @group representative_image
 */
class RepresentativeImageViewsTest extends RepresentativeImageTestBase {

  /**
   * Creates a view with two content types sharing a representative image field.
   */
  public function testViewsIntegration() {
    // Create a second content type.
    $this->drupalCreateContentType(['type' => 'article2', 'name' => 'Article2']);
    $this->createImageField('field_image3', 'article2');
    $this->createImageField('field_image4', 'article2');

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Add the existing representative image field. Set it to field3.
    $this->drupalGet('admin/structure/types/manage/article2/fields/add-field');
    $page->fillField('existing_storage_name', 'field_representative_image');
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save and continue');
    $edit = [
      'settings[representative_image_field_name]' => 'field_image3',
    ];
    $this->submitForm($edit, 'Save settings');

    // Create image files to use for testing.
    $image1 = $this->randomFile('image');
    $image2 = $this->randomFile('image');
    $image3 = $this->randomFile('image');
    $image4 = $this->randomFile('image');

    // Create a node of each content type.
    $this->drupalGet('node/add/article');
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->attachFileToField('files[field_image1_0]', $this->fileSystem->realpath($image1->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image1_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image1[0][alt]', $this->randomMachineName());
    $result = $assert_session->waitForElementVisible('css', '[name="files[field_image2_0]"]:enabled');
    $this->assertNotEmpty($result);
    $page->attachFileToField('files[field_image2_0]', $this->fileSystem->realpath($image2->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image2_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image2[0][alt]', $this->randomMachineName());
    $page->pressButton('Save');

    $this->drupalGet('node/add/article2');
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->attachFileToField('files[field_image3_0]', $this->fileSystem->realpath($image3->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image3_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image3[0][alt]', $this->randomMachineName());
    $result = $assert_session->waitForElementVisible('css', '[name="files[field_image4_0]"]:enabled');
    $this->assertNotEmpty($result);
    $page->attachFileToField('files[field_image4_0]', $this->fileSystem->realpath($image4->uri));
    $result = $assert_session->waitForElementVisible('css', '[name="field_image4_0_remove_button"]');
    $this->assertNotEmpty($result);
    $page->fillField('field_image4[0][alt]', $this->randomMachineName());
    $page->pressButton('Save');

    // Create a view that lists all content using fields.
    $this->drupalGet('admin/structure/views/add');
    $page->findField('edit-label')->setValue('Sample view');
    $page->findField('edit-page-create')->check();
    $page->findField('edit-page-style-row-plugin')->setValue('fields');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $page->pressButton('Save and edit');
    $page->findField('id')->setValue('sample_view');
    $page->pressButton('Save and edit');

    // Add the representative image field to the view.
    $this->assertNotEmpty($assert_session->waitForElementVisible('css', '#views-add-field'));
    $this->click('#views-add-field');
    $assert_session->assertWaitOnAjaxRequest();
    $page->findField('override[controls][options_search]')->setValue('representative');
    $page->findField('name[node__field_representative_image.field_representative_image]')->check();
    // The Save and continue button has a weird structure.
    $page->find('css', 'div > button.button.button--primary.js-form-submit.form-submit.ui-button.ui-corner-all.ui-widget')->click();
    $assert_session->assertWaitOnAjaxRequest();

    // Set the representative image's image style and link properties.
    $page->findField('options[settings][image_style]')->setValue('medium');
    $page->findField('options[settings][image_link]')->setValue('content');
    // The Apply button has a weird structure.
    $page->find('css', 'div > button.button.button--primary.js-form-submit.form-submit.ui-button.ui-corner-all.ui-widget')->click();
    $assert_session->assertWaitOnAjaxRequest();

    // Save the view.
    $page->pressButton('edit-actions-submit');
    $assert_session->assertWaitOnAjaxRequest();

    // Open the view and check that the right fields are being shown.
    $this->drupalGet('sample-view');
    $this->assertImage($image1);
    $this->assertImage($image3);

    // 2. Switch the representative image and confirm the representative image
    // is being replaced properly.
    $edit = [
      'settings[representative_image_field_name]' => 'field_image2',
    ];
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_representative_image');
    $this->submitForm($edit, 'Save settings');
    // 2. Set the second image field of Sample content type 2 as representative.
    $edit = [
      'settings[representative_image_field_name]' => 'field_image4',
    ];
    $this->drupalGet('admin/structure/types/manage/article2/fields/node.article2.field_representative_image');
    $this->submitForm($edit, 'Save settings');
    // @todo Check why clearing caches is needed in order to find
    //   field_representative_image in the node.
    drupal_flush_all_caches();

    // Open the view and check that the right fields are being shown.
    $this->drupalGet('sample-view');
    $this->assertImage($image2);
    $this->assertImage($image4);
  }

}
